package com.mash.paris.investmentbook.listeners;

import com.mash.paris.investmentbook.config.MessageChannels;
import lombok.extern.slf4j.Slf4j;
import org.mash.paris.domain.Activity;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.kafka.support.Acknowledgment;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.stereotype.Service;

@Service
@Slf4j
//@EnableBinding(MessageChannels.class)
public class ActivityEventProcessor {
//    @StreamListener(target = MessageChannels.ACTIVITY_EVENT_IN)
//    public void consumeAction(Message<Activity> message, @Header(KafkaHeaders.RECEIVED_PARTITION_ID) String partitionId, @Header(KafkaHeaders.OFFSET) String offset, @Header(KafkaHeaders.ACKNOWLEDGMENT) Acknowledgment ack) {
//        System.out.println("Recv message is :  " + message);
//
//    }
}
