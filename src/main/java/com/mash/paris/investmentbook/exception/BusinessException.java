package com.mash.paris.investmentbook.exception;

public class BusinessException extends Exception {
    public BusinessException(String message) {
        super(message);
    }

    public BusinessException(String mesage, Throwable clause) {
        super(mesage, clause);
    }

}
