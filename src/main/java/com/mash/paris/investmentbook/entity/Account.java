package com.mash.paris.investmentbook.entity;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.mash.paris.investmentbook.enums.Types;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Setter
@Data
@Entity
@EntityListeners(AuditingEntityListener.class)
@EqualsAndHashCode
@Table(name = "ACCOUNT", uniqueConstraints = @UniqueConstraint(columnNames = "ACCOUNT_ID"))
public class Account {
    @Id
    @Column(name = "ACCOUNT_ID")
    private String accountId;
    @EqualsAndHashCode.Exclude
    @Enumerated(EnumType.STRING)
    @Column(name = "COST_METHOD")
    private Types.CostMethod costMethod;
    @Column(name = "AUDIT_CREATE_TS", updatable = false)
    @CreationTimestamp
    private LocalDateTime auditCreateTs;
    @Column(name = "ACCOUNT_CD")
    private String accountCd;
    @Column(name = "PROCESS_DATE")
    private LocalDate processDate;
    @Column(name = "AUDIT_UPDATE_TS", updatable = false)
    @UpdateTimestamp
    private LocalDateTime auditUpdateTs;
}
