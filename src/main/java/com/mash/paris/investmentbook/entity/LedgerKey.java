package com.mash.paris.investmentbook.entity;

import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Objects;
@NoArgsConstructor
public class LedgerKey implements Serializable {
    private String ledgerId;
    private Integer ledgerVersion;
    private LocalDate impactDate;

    public LedgerKey(String ledgerId, Integer ledgerVersion, LocalDate impactDate) {
        this.impactDate = impactDate;
        this.ledgerId = ledgerId;
        this.ledgerVersion = ledgerVersion;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) return true;
        if (obj == null || getClass() != obj.getClass()) return false;
        LedgerKey ledgerKey = (LedgerKey) obj;
        return Objects.equals(ledgerId, ledgerKey.ledgerId) && Objects.equals(ledgerVersion, ledgerKey.ledgerVersion) && Objects.equals(impactDate, ledgerKey.impactDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ledgerId, impactDate, ledgerVersion);
    }
}
