package com.mash.paris.investmentbook.entity;

import com.mash.paris.investmentbook.enums.Types;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@EntityListeners(AuditingEntityListener.class)
@Entity
@EqualsAndHashCode(of = {"ledgerId", "ledgerVersion", "impactDate"})
@Table(name = "LEDGER")
@IdClass(LedgerKey.class)
public class Ledger {
    @Id
    @Column(name = "LEDGER_ID", updatable = false)
    private String ledgerId;
    @Id
    @Column(name = "VERSION", updatable = false)
    private Integer ledgerVersion;
    @Id
    @Column(name = "IMPACT_DATE", updatable = false)
    private LocalDate impactDate;
    @Column(name = "ACCOUNT_ID", updatable = false)
    private String accountId;
    @Enumerated(EnumType.STRING)
    @Column(name = "BOOK_TYPE", updatable = false)
    private Types.BookType bookType;
    @Enumerated(EnumType.STRING)
    @Column(name = "ACTIVE", updatable = false)
    private Types.ActiveType active;
    @Column(name = "TRANSITION_ID", updatable = false)
    private String transitionId;
    @Column(name = "ACTIVITY_ID", updatable = false)
    private String activityId;
    @Column(name = "INSTRUMENT_ID", updatable = false)
    private String instrumentId;
    @Column(name = "BALANCE")
    private BigDecimal balance;
    @CreationTimestamp
    @Column(name = "CREATED_TS")
    private LocalDateTime createdTime;
    @UpdateTimestamp
    @Column(name = "UPDATED_TS")
    private LocalDateTime updatedTime;
}
