package com.mash.paris.investmentbook.config;

import com.google.common.base.Strings;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Properties;

@Configuration
@Slf4j
@EnableJpaRepositories(entityManagerFactoryRef = "entityManagerFactory", basePackages = {"com.mash.paris.investmentbook.repository"})
@Profile({"local"})
public class LocalDataSourceConfig {
    @Value("${invest.datasrc.oracle.username}")
    private String user;
    @Value("${invest.datasrc.oracle.password}")
    private String password;
    @Value("${invest.datasrc.oracle.jdbcUrl}")
    private String jdbcUrl;
    @Value("${invest.datasrc.oracle.driverClass}")
    private String driverClass;

    @Bean(name = "dataSource")
    public DataSource dataSource() {
        HikariConfig cfg = new HikariConfig();
        if (!Strings.isNullOrEmpty(user) && !Strings.isNullOrEmpty(password)) {
            cfg.setUsername(user);
            cfg.setPassword(password);
        }
        cfg.setJdbcUrl(jdbcUrl);
        cfg.setDriverClassName(driverClass);
        return new HikariDataSource(cfg);
    }

    @Bean(name = "entityManagerFactory")
    public LocalContainerEntityManagerFactoryBean entityManagerFactoryBean(DataSource dataSource) throws SQLException {
        Properties jpaProperties = new Properties();
        jpaProperties.setProperty("hibernate.show_sql", "true");
//        if (!Strings.isNullOrEmpty(default_schema)) {
//            jpaProperties.setProperty("hibernate.default_schema", default_schema);
//        }
        HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        if (jdbcUrl.startsWith("jdbc:h2")) {
            vendorAdapter.setGenerateDdl(true);
        }
        LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
        factoryBean.setJpaVendorAdapter(vendorAdapter);
        factoryBean.setPackagesToScan("com.mash.paris.investmentbook.entity");
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaProperties(jpaProperties);
        return factoryBean;
    }

    @Bean(name = "transactionManager")
    public PlatformTransactionManager platformTransactionManager(SessionFactory sessionFactory) {
        HibernateTransactionManager mgr = new HibernateTransactionManager(sessionFactory);
        mgr.setNestedTransactionAllowed(true);
        return mgr;
    }

    @Bean(name = "jdbcTemplate")
    public JdbcTemplate jdbcTemplate(final DataSource dataSource) {
        return new JdbcTemplate(dataSource);
    }
}
