package com.mash.paris.investmentbook.repository;

import com.mash.paris.investmentbook.entity.Ledger;
import com.mash.paris.investmentbook.entity.LedgerKey;
import com.mash.paris.investmentbook.enums.Types;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.Optional;

@Repository
public interface LedgerRepository extends JpaRepository<Ledger, String> {

    @Query(value = "select l from Ledger l where l.accountId= :accountId and (:instrumentId is null or l.instrumentId= :instrumentId) " +
            " and (:active is null or l.active= :active) and (:impactDate is null or l.impactDate= :impactDate)" +
            " and (:activityId is null or l.activityId= :activityId) and (:transitionId is null or l.transitionId= :transitionId)" +
            " and (:ledgerId is null or l.ledgerId= :ledgerId)")
    Page<Ledger> getLedgerByInputs(String accountId,
                                   Optional<String> instrumentId,
                                   Optional<Types.ActiveType> active,
                                   Optional<LocalDate> impactDate,
                                   Optional<String> activityId,
                                   Optional<String> transitionId,
                                   Optional<String> ledgerId,
                                   Pageable pageable);
}
