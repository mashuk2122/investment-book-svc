package com.mash.paris.investmentbook.repository;

import com.mash.paris.investmentbook.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<Account,String> {

}
