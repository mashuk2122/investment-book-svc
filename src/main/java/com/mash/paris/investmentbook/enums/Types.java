package com.mash.paris.investmentbook.enums;

public final class Types {
    private Types() {

    }

    public enum CostMethod {
        AvgCost,
        ProRata,
        HighCost,
        LowCost,
        FIFO,
        LIFO,
        BestTax
    }

    public enum ActiveType {
        EOD,
        RT,
        AUDIT
    }

    public enum BookType {
        Gaap,
        Tax
    }
}
