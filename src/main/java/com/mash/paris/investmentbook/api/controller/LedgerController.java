package com.mash.paris.investmentbook.api.controller;

import com.mash.paris.investmentbook.entity.Ledger;
import com.mash.paris.investmentbook.enums.Types;
import com.mash.paris.investmentbook.repository.LedgerRepository;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDate;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Controller
public class LedgerController {
    @Value("${page.size.max.allowed}")
    private String maxPageSizeAllowed;
    @Autowired
    LedgerRepository ledgerRepository;

    @GetMapping("getLedgersByPreferredInput")
    @ApiOperation(httpMethod = "GET", value = "Paginated Api. Flexibility options : user can provide sort-by field name, sort-direction, total pages required at a time (max allowed 2000), all i/p fields are optional except accountId, user can provide more i/ps for filtered result")
    public ResponseEntity<Map<String, Object>> getLedgersByPreferredInput(
            @RequestParam(defaultValue = "0") @ApiParam("Default page number starts at 0") int pageNumber,
            @RequestParam(defaultValue = "100") @ApiParam("Default page size is 100, max allowed 2000") Integer pageSize,
            @RequestParam(name = "pageToBeSortedBy", required = false) @ApiParam(value = "Optional") LedgerSorter sortBy,
            @RequestParam(name = "sortDirection", defaultValue = "ASC") @ApiParam(value = "Optional") FlowDirection sortDirection,
            @RequestParam(name = "accountId") @ApiParam(value = "Required", required = true) String accountId,
            @RequestParam(name = "impactDate") @ApiParam(value = "Optional") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Optional<LocalDate> impactDate,
            @RequestParam(name = "transitionId") @ApiParam(value = "Optional") Optional<String> transitionId,
            @RequestParam(name = "activityId") @ApiParam(value = "Optional") Optional<String> activityId,
            @RequestParam(name = "activityId") @ApiParam(value = "Optional") Optional<String> ledgerId,
            @RequestParam(name = "instrumentId") @ApiParam(value = "Optional") Optional<String> instrumentId,
            @RequestParam(name = "activeType", required = false) Optional<Types.ActiveType> activeType
    ) {
        Map<String, Object> response = new HashMap<>();
        Page<Ledger> pagedLedgers = null;
        Pageable pageable = null;
        int maxPageSize = maxPageSizeAllowed != null ? pageSize > Integer.parseInt(maxPageSizeAllowed) ? 2000 : Integer.parseInt(maxPageSizeAllowed) : 1000;
        if (sortBy != null) {
            Sort sort = sortDirection.name().equals(Sort.Direction.ASC.name()) ? Sort.by(sortBy.name()).ascending() : Sort.by(sortBy.name()).descending();
            pageable = PageRequest.of(pageNumber, pageSize, sort);
        } else {
            pageable = PageRequest.of(pageNumber, pageSize);
        }
        pagedLedgers = ledgerRepository.getLedgerByInputs(accountId, instrumentId, activeType, impactDate, activityId, transitionId, ledgerId, pageable);
        if (pagedLedgers != null) {
            response.put("currentPage", pagedLedgers.getNumber());
            response.put("totalNumberOfRecords", pagedLedgers.getTotalElements());
            response.put("totalPages", pagedLedgers.getTotalPages());
            response.put("ledgers", pagedLedgers.getContent());
        }
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    public enum LedgerSorter {
        ledgerVersion,
        impactDate,
        createdTs,
        updatedTs
    }

    public enum FlowDirection {
        ASC,
        DESC
    }
}
