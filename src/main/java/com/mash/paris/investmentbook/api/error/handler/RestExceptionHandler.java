package com.mash.paris.investmentbook.api.error.handler;

import com.mash.paris.investmentbook.api.error.ApiErrorResponse;
import com.mash.paris.investmentbook.exception.BusinessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.stream.Collectors;

@RestControllerAdvice
public class RestExceptionHandler {
    @ExceptionHandler(BusinessException.class)
    public ResponseEntity<Object> handleBusinessException(BusinessException exception) {
        ApiErrorResponse errRes = new ApiErrorResponse.ApiErrorResponseBuilder()
                .withMessage(exception.getMessage())
                .atTime(LocalDateTime.now(ZoneOffset.UTC))
                .withDetail("Business Exception Occurred")
                .build();
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(errRes);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Object> handleValidationException(MethodArgumentNotValidException validationException, HttpHeaders headers, HttpStatus status, WebRequest request) {
        List<String> errorMsg = validationException.getBindingResult().getFieldErrors().stream().map(e -> e.getDefaultMessage()).collect(Collectors.toList());
        ApiErrorResponse errRes = new ApiErrorResponse.ApiErrorResponseBuilder().
                withMessage(errorMsg.toString())
                .withDetail("Validation failure")
                .withStatus(status)
                .atTime(LocalDateTime.now(ZoneOffset.UTC))
                .build();
        return ResponseEntity.status(status).body(errRes);
    }
}
