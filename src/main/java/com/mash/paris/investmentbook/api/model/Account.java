package com.mash.paris.investmentbook.api.model;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.mash.paris.investmentbook.enums.Types;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;

@Data
@Builder
public class Account implements Serializable {
    @NotNull(message = "accountId can't be null")
    private String accountId;
    @NotNull(message = "accountCd can't be null")
    private String accountCd;
    @NotNull(message = "costMethod can't be null")
    private Types.CostMethod costMethod;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate processDate;
}
