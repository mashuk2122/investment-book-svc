package com.mash.paris.investmentbook.api.controller;

import com.mash.paris.investmentbook.api.model.Account;
import com.mash.paris.investmentbook.exception.BusinessException;
import com.mash.paris.investmentbook.repository.AccountRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Optional;

@RestController
@Slf4j
public class AccountController {
    @Autowired
    AccountRepository accountRepository;

    @PostMapping("updateAccount")
    public ResponseEntity<com.mash.paris.investmentbook.entity.Account> updateAccount(@RequestBody @Valid Account accountMdl) throws BusinessException {
        com.mash.paris.investmentbook.entity.Account accountEntity = new com.mash.paris.investmentbook.entity.Account();
        BeanUtils.copyProperties(accountMdl, accountEntity);
        if (accountEntity != null) {
            accountRepository.findById(accountEntity.getAccountId()).orElseThrow(
                    () -> new BusinessException("Account does not exist")
            );
            accountEntity = accountRepository.save(accountEntity);
        }
        return ResponseEntity.status(HttpStatus.OK).body(accountEntity);
    }

    @PostMapping("addAccount")
    public ResponseEntity<com.mash.paris.investmentbook.entity.Account> addAccount(@RequestBody @Valid Account accountMdl) throws BusinessException {
        com.mash.paris.investmentbook.entity.Account accountEntity = new com.mash.paris.investmentbook.entity.Account();
        BeanUtils.copyProperties(accountMdl, accountEntity);
        if (accountEntity != null) {
            if (accountRepository.findById(accountEntity.getAccountId()).isPresent()) {
                throw new BusinessException("Account Already Exist");
            }
            accountEntity = accountRepository.save(accountEntity);
        }
        return ResponseEntity.status(HttpStatus.OK).body(accountEntity);
    }

    @GetMapping("getAccountById")
    public ResponseEntity<com.mash.paris.investmentbook.entity.Account> getAccount(@RequestParam("accountId") String accountId) {
        Optional<com.mash.paris.investmentbook.entity.Account> accountEntity = accountRepository.findById(accountId);
        if (accountEntity.isPresent()) {
            return ResponseEntity.status(HttpStatus.OK).body(accountEntity.get());
        }
        return ResponseEntity.status(HttpStatus.OK).body(new com.mash.paris.investmentbook.entity.Account());
    }
}
