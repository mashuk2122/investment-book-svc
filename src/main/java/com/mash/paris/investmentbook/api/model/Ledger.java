package com.mash.paris.investmentbook.api.model;

import com.mash.paris.investmentbook.enums.Types;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;

public class Ledger {
    private String ledgerId;
    private String ledgerVersion;
    private LocalDate impactDate;
    private String accountId;
    private Types.BookType bookType;
    private Types.ActiveType active;
    private String transitionId;
    private String activityId;
    private BigDecimal balance;
    private String instrumentId;
    private LocalDateTime createdTs;
    private LocalDateTime updatedTs;

}
